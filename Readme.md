*** Library Management System in Django ***

This is a library management system for LetsRead@IITBHU, Books are donoted by members.

System Requirements:
	python==2.7+
	amqp==1.4.7
	anyjson==0.3.3
	billiard==3.3.0.21
	celery==3.1.19
	Django==1.8.4
	django-crispy-forms==1.5.2
	kombu==3.0.29
	oauthlib==1.0.3
	Pillow==3.0.0
	PyJWT==1.4.0
	python-openid==2.2.5
	python-social-auth==0.2.13
	pytz==2015.7
	requests==2.8.1
	requests-oauthlib==0.5.0
	six==1.10.0
	wheel==0.24.0
	
How to setup:
	Install the requirements.txt file using pip install (preferably in a virtual environment)
	
How to run local server:
	in dir ./vproj/ run 'python manage.py runserver' (portno optional) from terminal.
	This will run the local server on portno (8000 by default), which can be accessed on url 'http://127.0.0.1/'
	
Features:
	The current library management system allows for 2 different user logins - Admin (or librarian) & User (or patron)
	
#Admin features:
	1) Add/Delete books and book copies
	2) Add/Delete members/users/patrons
	3) Add/Delete publishers
	4) Add/Delete authors
	5) Add/Delete categories/tags for books
	6) Add/Delete author , publisher, category info for book
	7) View Issued History
	8) Issue a Book
	9) Return a Book
	10) View Due Books (sorted by due_date)
	
#User features:
	1) View all Books in Library
	2) View all his books (issued and returned)
	3) View all currently issued books
	4) View all due books
	
Project Structure:
	./templates folder contains all the html pages
	./static_in_pro folder contains all the static files like .js ,  .css & .jpg files
	./library/models.py contains all the Database table information
	./library/views.py contains all the view functions join the url and the backend
	./vproj/urls.py contains all the valid url patterns
	
* All users and admin login via google sign in (can be extended to custom login as well if needed )
* Admin are verified from list 'ADMIN_EMAILS' in views.py
* Members are verfied from Member table in database where email_id is unique
* When a book is issued a mail to user and admin is sent stating the book issued and the due date
	

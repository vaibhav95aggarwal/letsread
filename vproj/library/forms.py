from django import forms

from .models import Category
from .models import Author
from .models import Publisher
from .models import Book
from .models import HasCategory
from .models import CompiledBy
from .models import Member
from .models import BookCopy
from .models import History
from .models import WaitingList

import datetime

class CategoryForm(forms.ModelForm):
	class Meta:
		model = Category
		fields = ['name']

	def clean_name(self):
		name = self.cleaned_data.get('name')
		if len(name) == 0:
			raise forms.ValidationError("Category name can't be empty")
		return name

class AuthorForm(forms.ModelForm):
	class Meta:
		model = Author
		fields = ['first_name','last_name']

class PublisherForm(forms.ModelForm):
	class Meta:
		model = Publisher
		fields = ['name']

class BookForm(forms.ModelForm):
	class Meta:
		model = Book
		fields = ['name','isbn_no','rating']

	# def clean_no_of_copies(self):
	# 	c = self.cleaned_data.get('no_of_copies')
	# 	if c < 0:
	# 		raise forms.ValidationError("Enter non negative no of copies")
	# 	return c

	def clean_isbn_no(self):
		no = self.cleaned_data.get('isbn_no')
		if no < 0:
			raise forms.ValidationError("Enter non negative isbn no")
		return no

	def clean_rating(self):
		rating = self.cleaned_data.get('rating')
		if rating < 0 & rating > 5:
			raise forms.ValidationError("Enter rating from 0 to 5")
		return rating

class BookCopyForm(forms.ModelForm):
	class Meta:
		model = BookCopy
		fields = ['book_id','copy_id','availability','donor_id']

	def clean_availability(self):
		avail = self.cleaned_data.get('availability')
		print avail
		if not avail:
			raise forms.ValidationError("A new copy cant be issued already! Set correct availability")
		return avail
		
class HasCategoryForm(forms.ModelForm):
	class Meta:
		model = HasCategory
		fields = ['book_id','category_id']

class CompiledByForm(forms.ModelForm):
	class Meta:
		model = CompiledBy
		fields = ['book_id','author_id','publisher_id']

class MemberForm(forms.ModelForm):
	class Meta:
		model = Member
		fields = ['first_name','last_name','phone','email','date_of_joining','reference_id']

	def clean_email(self):
		email = self.cleaned_data.get('email')
		domain = email.split('@')[1]
		if domain == 'iitbhu.ac.in':
			domain = 'itbhu.ac.in'
			email = email.split('@')[0] + '@' + domain
		print "entered email " + email
		return email

class HistoryForm(forms.ModelForm):
	class Meta:
		model = History
		fields = ['book_id','copy_id','member_id','issue_date','due_date','return_date']

	def clean_issue_date(self):
		issue_date = self.cleaned_data.get('issue_date')
		now = datetime.datetime.now().date()
		if issue_date >= now:
			# validDate
			return issue_date
		else:
			raise forms.ValidationError("Enter issue date after current date")

	def clean_due_date(self):
		due_date = self.cleaned_data.get('due_date')
		issue_date = self.cleaned_data.get('issue_date')
		if issue_date is not None and due_date > issue_date:
			# validDate
			return due_date
		else:
			raise forms.ValidationError("due date must be after issue date")

class IssueForm(forms.ModelForm):
	class Meta:
		model = History
		fields = ['book_id','copy_id','member_id','issue_date','due_date']

	def clean_copy_id(self):
		copy = self.cleaned_data.get('copy_id')
		# print "copy " + str(copy)
		book = self.cleaned_data.get('book_id')
		# print book.book_id
		if book == copy.book_id:
			# correct book
			# check availability
			# print type(copy)
			available = (BookCopy.objects.get(book_id__book_id = book.book_id ,copy_id=copy.copy_id)).availability
			print str(available) + " issued"
			if available:
				print "correct"
				return copy
			else:
				raise forms.ValidationError("This copy already issued. Select another")
		else:
			print "error except"
			raise forms.ValidationError("Select copy for the correct book")

	def clean_member_id(self):
		member_id = self.cleaned_data.get('member_id')
		hasAnotherBook = False
		if not hasAnotherBook:
			return member_id
		else:
			raise forms.ValidationError("Member has already issued a book")

	def clean_issue_date(self):
		issue_date = self.cleaned_data.get('issue_date')
		now = datetime.datetime.now().date()
		if issue_date >= now:
			# validDate
			return issue_date
		else:
			raise forms.ValidationError("Enter issue date after current date")

	def clean_due_date(self):
		due_date = self.cleaned_data.get('due_date')
		issue_date = self.cleaned_data.get('issue_date')
		if issue_date is not None and due_date > issue_date:
			# validDate
			return due_date
		else:
			raise forms.ValidationError("due date must be after issue date")

class WaitingListForm(forms.ModelForm):
	class Meta:
		model = WaitingList
		fields = ['book_id','member_id','waiting_no']


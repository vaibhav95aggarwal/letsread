from django.shortcuts import render_to_response, redirect, render
from django.http import JsonResponse
from .forms import CategoryForm
from .forms import AuthorForm
from .forms import PublisherForm
from .forms import BookForm
from .forms import HasCategoryForm
from .forms import CompiledByForm
from .forms import MemberForm
from .forms import BookCopyForm
from .forms import HistoryForm
from .forms import IssueForm
from .models import Category
from .models import Author
from .models import Publisher
from .models import Book
from .models import HasCategory
from .models import CompiledBy
from .models import Member
from .models import BookCopy
from .models import History

from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.db.models import F
from django.core.mail import send_mail
from django.conf import settings



import datetime


ADMIN_EMAILS = { 'vaibhav95aggarwal@gmail.com', 'letsread.iitbhu@gmail.com', 'meetakshi17@gmail.com' }

def about_us(request):

	return render(request,'about_us.html')

# Create your views here.
@login_required(login_url='/login')
def category(request):
	if request.session["admin"]:
		form = CategoryForm(request.POST or None)

		context = {
			"text": "Enter Book Category Details ",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)

			instance.save()
			context = {
				"text": "Thank You"
			}
			return redirect("view_categories")
		return render(request,"category.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def author(request):
	if request.session["admin"]:
		form = AuthorForm(request.POST or None)

		context = {
			"text": "Enter Book Author Details ",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit = False)
			instance.save()

			context = {
				"text": "Thank You"
			}
			return redirect("view_authors")

		return render(request,"author.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def publisher(request):
	if request.session["admin"]:
		form = PublisherForm(request.POST or None)

		context = {
			"text": "Enter Book Publisher Details ",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit = False)
			instance.save()

			context = {
				"text": "Thank You"
			}
			return redirect("view_publishers")
		return render(request,"publisher.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def book(request):
	if request.session["admin"]:
		form = BookForm(request.POST or None)

		context = {
			"text": "Enter Book Details",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()

			context = {
				"text": "Thank You"	
			}
			return redirect("view_books")
		return render(request, "book.html", context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def has_category(request):
	if request.session["admin"]:
		form = HasCategoryForm(request.POST or None)

		context = {
			"text": "Enter values",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()

			context = {
				"text": "Thank You"	
			}
			return redirect("view_book_categories")
		return render(request, "has_category.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def compiled_by(request):
	if request.session["admin"]:
		form = CompiledByForm(request.POST or None)

		context = {
			"text": "Enter Values",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()

			context = {
				"text": "Thank You"
			}
			return redirect("view_compiled_by")
		return render(request,"compiled_by.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def member(request):
	if request.session["admin"]:
		form = MemberForm(request.POST or None)

		context = {
			"text": "Add a member",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			instance.save()

			context = {
				"text": "Thank You"
			}
			return redirect("view_members")
		return render(request,"member.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def book_copy(request):
	if request.session["admin"]:
		form = BookCopyForm(request.POST or None)
		context = {
			"text": "Add a copy of Book",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			# print "submitted" + form	
			# increase a no_of_copies in book
			Book.objects.filter(book_id = form.cleaned_data.get('book_id').book_id).update(no_of_copies=F('no_of_copies')+1)
			print book
			instance.save()
			context = {
				"text": "Thank You"
			}
			return redirect("view_books")
		return render(request,"book_copy.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login')
def history(request):
	if request.session["admin"]:
		form = HistoryForm(request.POST or None)
		# print "form" + str(form)
		context = {
			"text": "Change History Details",
			"form": form,
		}

		if form.is_valid():
			instance = form.save(commit=False)
			# print "submitted" + form
			print 'here'
			print instance
			instance.save()

			context = {
				"text": "Thank You"
			}
		return render(request,"history.html",context)
	else:
		return redirect("dashboard")

def login(request):
	print request.path
	if not request.user.is_anonymous():
		return checklogin(request)
	return render(request,'login.html')

@login_required(login_url='/login')
def logout(request):
	if 'admin' in request.session:
		del request.session['admin']
	auth_logout(request)
	context = {

		}
	return render(request, 'base.html', context)

def checklogin(request):
	if request.user.is_anonymous():
		return render(request, 'login.html')
	else :
		# check if valid mail
		curr_email = request.user.email
		print "curr_email " + curr_email
		domain = curr_email.split('@')[1]

		# if domain == 'itbhu.ac.in' :
		# 	curr_email2 = curr_email.split('@')[0] + '@' + 'iitbhu.ac.in'
		# elif domain == 'iitbhu.ac.in':
		# 	curr_email2 = curr_email.split('@')[0] + '@' + 'itbhu.ac.in'

		try:
			valid_user = Member.objects.get(email = curr_email)
			print "valid user " + str(valid_user.email)
		except Member.DoesNotExist:
			print "invalid" + request.user.email
			# google signed in need to sign out
			auth_logout(request)
			return render(request,"invalid.html")

		if curr_email in ADMIN_EMAILS:
			print "admin"
			request.session['admin'] = True
		else :
			print "not admin"
			request.session['admin'] = False

		context = {
			"is_admin": request.session['admin']
		}

		return render(request, 'dashboard_base.html',context)

@login_required(login_url='/login')
def dashboard(request):

	context = {
			"is_admin": request.session['admin']
	}
	return render(request, 'dashboard_base.html',context)

def view(request):
	db_category = Category.objects.all()
	db_author = Author.objects.all()
	db_publisher = Publisher.objects.all()
	db_book = Book.objects.all()
	db_has_category = HasCategory.objects.all()
	db_compiled_by = CompiledBy.objects.all()
	db_member = Member.objects.all()

	context = {
		"category": db_category,
		"author": db_author,
		"publisher": db_publisher,
		"book": db_book,
		"has_category": db_has_category,
		"compiled_by": db_compiled_by,
		"member": db_member,
	}

	return render(request, "view.html", context)

@login_required(login_url='/login/')
def view_books(request):
	if request.method == 'POST' :
		# check if book is valid
		print "POST"
		print request.POST
		if 'del_book_id' in request.POST:
			bookTobeDeleted = Book.objects.get(book_id = request.POST['del_book_id'])
			bookTobeDeleted.delete()
		if 'del_bookcopy_id' in request.POST:
			book = request.POST['del_bookcopy_id'].split('@')[0]
			copy = request.POST['del_bookcopy_id'].split('@')[1]
			bookcopyTobeDeleted = BookCopy.objects.get(book_id = book ,copy_id= copy)
			bookcopyTobeDeleted.delete()
			Book.objects.filter(book_id = book ).update(no_of_copies=F('no_of_copies')-1)

	db_book = Book.objects.all()
	db_book_copy = BookCopy.objects.all()

	context = {
		"book": db_book,
		"bookcopy": db_book_copy,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_books.html", context)

@login_required(login_url='/login/')
def view_members(request):
	if request.method == 'POST' :
		referer = False
		# check if member is referer
		if not referer:
			memberTobeDeleted = Member.objects.get(member_id = request.POST['del_id'])
			memberTobeDeleted.delete()

	db_member = Member.objects.all()

	context = {
		"member": db_member,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_members.html", context)

@login_required(login_url='/login/')
def view_publishers(request):
	if request.method == 'POST' :
		# check if publisher is valid
		publisherTobeDeleted = Publisher.objects.get(publisher_id = request.POST['del_id'])
		publisherTobeDeleted.delete()

	db_publisher = Publisher.objects.all()

	context = {
		"publisher": db_publisher,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_publishers.html", context)

@login_required(login_url='/login/')
def view_authors(request):
	if request.method == 'POST' :
		# check if author is valid
		authorTobeDeleted = Author.objects.get(author_id = request.POST['del_id'])
		authorTobeDeleted.delete()

	db_author = Author.objects.all()

	context = {
		"author": db_author,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_authors.html", context)

@login_required(login_url='/login/')
def view_categories(request):
	if request.method == 'POST' :
		# check if category is valid
		categoryTobeDeleted = Category.objects.get(category_id = request.POST['del_id'])
		categoryTobeDeleted.delete()

	db_category = Category.objects.all()

	context = {
		"category": db_category,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_categories.html", context)

@login_required(login_url='/login/')
def view_book_categories(request):
	if request.method == 'POST' :
		# check if book&category is valid
		bookandCategoryTobeDeleted = HasCategory.objects.get(book_id = request.POST['del_id'].split('@')[0], category_id =request.POST['del_id'].split('@')[1])
		bookandCategoryTobeDeleted.delete()

	db_has_category = HasCategory.objects.all().order_by('book_id')

	context = {
		"has_category": db_has_category,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_book_categories.html", context)

@login_required(login_url='/login/')
def view_compiled_by(request):
	if request.method == 'POST' :
		# check if compiledBy id valid
		book = request.POST['del_id'].split('@')[0]
		author = request.POST['del_id'].split('@')[1]
		publisher = request.POST['del_id'].split('@')[2]
		compiledByTobeDeleted = CompiledBy.objects.get(book_id = book, author_id=author,publisher_id=publisher)
		compiledByTobeDeleted.delete()

	db_compiled_by = CompiledBy.objects.all().order_by('book_id')

	context = {
		"compiled_by": db_compiled_by,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_compiled_by.html", context)

@login_required(login_url='/login/')
def view_history(request):

	if request.method == 'POST' :
		# check if history is valid
		print request.POST
		# print "book " + book
		book = request.POST['del_id'].split('@')[0]
		copy = request.POST['del_id'].split('@')[1]
		member = request.POST['del_id'].split('@')[2]
		issue_date = request.POST['del_id'].split('@')[3]
		print " book " + book + " copy "+ copy + " member "+ member + " idate " + issue_date
		historyTobeDeleted = History.objects.get(book_id__book_id = book, copy_id__copy_id= copy , issue_date=issue_date)
		historyTobeDeleted.delete()

	db_history = History.objects.all().order_by('book_id')

	context = {
		"history": db_history,
		"is_admin": request.session['admin'],
	}

	return render(request, "view_history.html", context)

@login_required(login_url='/login/')
def issue_book(request):
	if request.session['admin']:
		form = IssueForm(request.POST or None)

		context = {
			"form": form,
		}

		if request.method == 'POST':
			print form['copy_id']

		if form.is_valid():
			instance = form.save(commit=False)
			# TODO decrease count and change availability
			instance.save()
			book = form.cleaned_data.get('book_id')
			copy = form.cleaned_data.get('copy_id')
			due_date = form.cleaned_data.get('due_date')
			BookCopy.objects.filter(book_id = book ,copy_id = copy.copy_id ).update(availability=False,ETA=due_date)
			print (BookCopy.objects.get(book_id = book ,copy_id = copy.copy_id )).availability
			print (BookCopy.objects.get(book_id = book ,copy_id = copy.copy_id )).ETA
			print " availability "
			# context = {
			# 	"text": "Thank You"
			# }

			# send email to user book issed and due date
			member_email = form.cleaned_data.get('member_id').email
			send_subject = 'New Book issued'
			send_message = "%s was issued and is due on %s"%( str(copy) , form.cleaned_data.get('due_date'))
			print member_email
			print send_subject
			print send_message
			print settings.EMAIL_HOST_USER
			send_mail(	send_subject,
					 	send_message,
					 	settings.EMAIL_HOST_USER,
					 	[settings.EMAIL_HOST_USER,member_email],
					 	fail_silently=False
					 )

			return redirect("view_history")	

		return render(request,"issue_book.html",context)
	else:
		redirect("dashboard")

@login_required(login_url='/login/')
def return_book(request):
	if request.session['admin']:
		curr_date = datetime.datetime.now().date()
		if request.method == 'POST':
			book = request.POST['return_id'].split('@')[0]
			copy = request.POST['return_id'].split('@')[1]
			member = request.POST['return_id'].split('@')[2]
			issue_date = request.POST['return_id'].split('@')[3]
			print "return book " + book + " copy "+ copy + " member "+ member + " idate " + issue_date
			curr_date = datetime.datetime.now().date()
			# modify return date
			History.objects.filter(book_id__book_id = book, copy_id__copy_id= copy , issue_date=issue_date).update(return_date=curr_date)
			# make available
			BookCopy.objects.filter(book_id__book_id = book, copy_id = copy).update(availability=True,ETA=None)

		db_due_books = History.objects.filter(due_date__gte=curr_date,return_date=None).order_by('due_date')

		context = {
			"history": db_due_books,
		}

		return render(request, "return_book.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login/')
def view_due_books(request):
	if request.session['admin']:
		curr_date = datetime.datetime.now().date()
		db_due_books = History.objects.filter(due_date__gte=curr_date,return_date=None).order_by('due_date')

		context = {
			"history": db_due_books,
			"is_admin": request.session['admin'],
		}

		return render(request, "view_due_books.html",context)
	else:
		curr_date = datetime.datetime.now().date()
		db_due_books = History.objects.filter(member_id__email=request.user.email,due_date__gte=curr_date,return_date=None).order_by('due_date')

		context = {
			"history": db_due_books,
			"is_admin": request.session['admin'],
		}

		return render(request, "view_due_books.html",context)

@login_required(login_url='/login/')
def current_issued(request):
	if not request.session["admin"]:
		member_email = request.user.email

		db_issued = History.objects.filter(member_id__email=member_email,return_date=None).order_by('due_date')

		context = {
			"issued" : db_issued,
			"is_admin": request.session['admin'],
		}
		return render(request,"current_issued.html",context)
	else:
		return redirect("dashboard")

@login_required(login_url='/login/')
def all_issued(request):
	if not request.session["admin"]:
		member_email = request.user.email

		db_issued = History.objects.filter(member_id__email=member_email).order_by('due_date')

		context = {
			"issued" : db_issued,
			"is_admin": request.session['admin'],
		}
		return render(request,"all_issued.html",context)
	else:
		return redirect("dashboard")

def home(request):
	return render(request, "base.html")

@login_required(login_url='/login/')
@csrf_exempt
def get_copy_id(request):
	if request.method == 'POST':
		post = request.POST
		data = {}
		# print post['book_id']
		data['copy'] = list(BookCopy.objects.filter(book_id__book_id = post['book_id']).values('copy_id','book_id__name'))
		# print data
		return JsonResponse(data)
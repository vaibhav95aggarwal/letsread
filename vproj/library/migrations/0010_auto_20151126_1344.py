# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0009_auto_20151126_1047'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='history',
            unique_together=set([('book_id', 'copy_id', 'issue_date')]),
        ),
    ]

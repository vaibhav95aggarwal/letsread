# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('library', '0010_auto_20151126_1344'),
    ]

    operations = [
        migrations.AlterField(
            model_name='history',
            name='return_date',
            field=models.DateField(null=True, blank=True),
        ),
    ]

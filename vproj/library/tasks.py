from celery.decorators import task
from celery.utils.log import get_task_logger

logger = get_task_logger(__name__)

from email.mime.text import MIMEText
import smtplib

from celery.task.schedules import crontab
from celery.decorators import periodic_task


@periodic_task(
    run_every=(crontab(minute='*/2')), # run every 2 minutes
    name="task_periodic",
    ignore_result=True
)
def testPeriodic():
	logger.info("Periodic task run")
	return True


@task(name="send_email_task")
def send_email_task(toEmail, subject, message):
    """sends an email when feedback form is filled successfully"""
    logger.info("Sent feedback email")
    return send_email(toEmail, subject, message)

def send_email(to=None, subject=None, message=None):
    """sends email from hairycode-noreply to specified destination

    :param to: string destination address
    :param subject: subject of email
    :param message: body of message

    :return: True if successful
    """
    # prep message
    fro="littlegenius95@gmail.com"
    msg = MIMEText(message)
    msg['Subject'] = subject
    msg['From'] = fro 
    msg['To'] = to

    # log desired message to info level log
    logger.info('Sending email from: %r, to: %r' % (fro, to))
    
    # send message
    s = smtplib.SMTP('smtp.gmail.com')
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login('littlegenius95', 'passwordforemailtesting')
    s.sendmail(fro, [to], msg.as_string())
    s.quit()
    return True